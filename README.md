# README #


### What is this repository for? ###

This is for a practice project from https://javascript30.com/, 
specifically the Day 19 project titled "Unreal Webcam Fun".


### How do I get set up? ###
Because this project uses your webcam, it needs to be routed through a site with secure origin, either https or localhost.
Thus you will need to set up a simple local server. If you don't have one already, this project includes a script 
to set one up using npm and browser-sync. To set up the local server:

1. Go to nodejs.org, download node.js and install it (if you don't already have it).
2. In your terminal, navigate to the project and type "npm install" - this installs the dependencies needed for
	the server script.
3. When npm install has finished, you can type "npm start" and it will start up the localhost server and open 
	index.html on localhost port 3000 or 3001. 

In order to test the different functions, you will need to modify the scripts.js function itself, under the 
setInterval function. Uncomment the function you want to try and comment out the rest. Buttons for the various filters
will be forthcoming in the near future, hopefully.